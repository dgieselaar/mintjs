/*global angular*/
(function ( ) {
	
	angular.module('MintJS.core')
		.factory('safeApply', [ function ( ) {
			
			var noop = angular.noop;
			
			return function ( scope, func ) {
				if(!func) {
					func = noop;
				}
								
				if(!scope.$$phase && !scope.$root.$$phase) {
					return scope.$apply(func);
				} else {
					return func();
				}
				
			};
			
		}]);
	
})();
