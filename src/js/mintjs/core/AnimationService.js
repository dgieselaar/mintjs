/*global angular,setTimeout,clearTimeout*/
(function ( ) {
	
	angular.module('MintJS.core')
		.factory('animationService', [ '$document', '$timeout', function ( $document, $timeout ) {
			
			var animationService = {},
				transitionEnd;
				
			transitionEnd = (function ( document ) {
				
				var el = document.createElement('div'),
					transEndEventNames = {
						'WebkitTransition': 'transitionend webkitTransitionEnd',
						'MozTransition': 'transitionend',
						'OTransition': 'oTransitionEnd otransitionend',
						'transition': 'transitionend'
					},
					key,
					eventName;
					
				for(key in transEndEventNames){
					if(el.style[key] !== undefined) {
						eventName = transEndEventNames[key];
						break;
					}
				}
				
				return eventName;
				
			})($document[0]);
			
			if(transitionEnd) {
				transitionEnd = 'transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd';
			}
			
			animationService.onEnd = function ( element, callback ) {
				var timeoutPromise,
					isTransitionRunning,
					cancel;
				
				cancel = function ( ) {
					element.unbind(transitionEnd, onEnd);
					if(timeoutPromise) {
						$timeout.cancel(timeoutPromise);
					}
				};
				
				function onEnd ( event ) {
					callback(event);
				}
					
				timeoutPromise = $timeout(function ( ) {
					timeoutPromise = null;
					if(!isTransitionRunning) {
						cancel();
					}
				}, 0, false);
				
				if(transitionEnd) {
					isTransitionRunning = true;
					element.bind(transitionEnd, onEnd);
				}
				
				return cancel;
			};
			
			return animationService;
			
		}]);
	
})();
