/*global angular*/
(function ( ) {
	
	angular.module('MintJS.core')
		.factory('windowUnloadService', [ '$rootScope', '$window', '_', 'safeApply', function ( $rootScope, $window, _, safeApply ) {
			
			var windowUnloadService = {},
				callbacks = [];
			
			function attemptExit ( e ) {
				var i = 0,
					l = callbacks.length,
					msg;
				
				for(; i < l; ++i) {
					msg = callbacks[i](e);
					if(msg) {
						return msg;
					}
				}
				
				return undefined;
			}
			
			windowUnloadService.register = function ( callback ) {
				callbacks.push(callback);
			};
			
			windowUnloadService.unregister = function ( callback ) {
				_.pull(callbacks, callback);
			};
			
			// Doesn't work, using jquery for now
			// $window.onbeforeunload = function ( e ) {
			// return attemptExit(e);
			// };
			
			angular.element($window).bind('beforeunload', function ( e) {
				return safeApply($rootScope, function ( ) {
					return attemptExit(e);
				});
			});
			
			return windowUnloadService;
			
		}]);
	
})();
