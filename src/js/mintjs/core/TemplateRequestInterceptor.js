/*global angular,_*/
(function ( ) {
	
	var ERR_NOT_FOUND = 'Targeted partial was not found in parent ',
		app = angular.module('MintJS.core');
	
	app.factory('templateRequestInterceptor', [ '$rootScope', '$injector', '$templateCache', '$q', '$document', function ( $rootScope, $injector, $templateCache, $q, $document ) {
			
			var $http;
			
			return {
				request: function ( config ) {
					var match,
						base,
						deferred,
						cached,
						httpPromise;
					
					// check if URL is a targeted partial
					match = config.url.match(/((.*?)\.html)\#(.*)$/);
					
					if(!match) {
						return config;
					}
					
					// check if partial is cached
					// return if it's already cached
					cached = $templateCache.get(config.url);
										
					if(cached) {
						return config;
					}
					
					deferred = $q.defer();
					
					base = match[1];
					
					function attemptResolve ( ) {
						if(!$templateCache.get(config.url)) {
							deferred.reject(ERR_NOT_FOUND);
						} else {
							deferred.resolve(config);
						}
					}
					
					function errorOut ( errMsg ) {
						deferred.reject(errMsg);
					}
					
					if(!$http) {
						$http = $injector.get('$http');
					}
					
					httpPromise = $http.get(base, { cache: $templateCache })
						.then(function ( response ) {
							
							var data = response.data,
								div,
								templates;
							
							if($templateCache.get(config.url)) {
								attemptResolve();
								return;
							}
							
							$templateCache.put(base, data);
								
							div = $document[0].createElement('div');
							div.innerHTML = data;
							
							templates = div.querySelectorAll('script[type="text/ng-template"]');
							
							_.each(templates, function ( tpl ) {
								$templateCache.put(tpl.getAttribute('id'), angular.element(tpl).text());
							});
							
							attemptResolve();
							
							
						})
						.catch(function ( err ) {
							errorOut(err);
						});
					
					return deferred.promise;
				}
			};
			
		}])
		.config([ '$httpProvider', function ( $httpProvider ) {
			$httpProvider.interceptors.push('templateRequestInterceptor');
		}]);
	
})();
