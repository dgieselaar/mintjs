/*global angular,describe, beforeEach, module, it, expect,inject*/
describe('animationService', function() {
	
	var $rootScope,
		$compile,
		$timeout,
		animationService,
		element;
		
	beforeEach(module('templates'));
	beforeEach(module('MintJS'));
	
	beforeEach(inject([ '$rootScope', '$compile', '$timeout', 'animationService', function ( ) {
		
		$rootScope = arguments[0];
		$compile = arguments[1];
		$timeout = arguments[2];
		animationService = arguments[3];
		
		element = angular.element('<div></div>');
		$compile(element)($rootScope);
		$rootScope.$digest();
		
	}]));
	
	it('should call when the animation is complete', function ( ) {
		
		var ended = false;
		
		animationService.onEnd(element, function ( ) {
			ended = true;
		});
		
		$timeout(function ( ) {
			element.triggerHandler('transitionend');
			expect(ended).toBe(true);
		});
		
		$timeout.flush();
		
		
	});
	
	it('should not call if canceled', function ( ) {
		
		var ended = false,
			cancel;
		
		cancel = animationService.onEnd(element, function ( ) {
			ended = true;
		});
		
		$timeout(function ( ) {
			element.triggerHandler('transitionend');
			expect(ended).toBe(false);
		});
		
		cancel();
		
		$timeout.flush();
		
		
	});
  
});
