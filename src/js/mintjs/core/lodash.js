/*global angular*/
(function ( ) {
	
	angular.module('MintJS.core')
		.factory('_', [ '$window', function ( $window ) {
			
			return $window._;
			
		}]);
	
})();
