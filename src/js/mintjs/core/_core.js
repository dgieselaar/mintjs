/*global angular*/
(function ( ) {
	
	angular.module('MintJS.core', [ 
		'ngAnimate'
	]);
	
})();