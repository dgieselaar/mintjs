/*global angular,describe, beforeEach, module, it, expect,inject*/
describe('TemplateRequestInterceptor', function() {
	
		
	beforeEach(module('templates'));
	beforeEach(module('MintJS'));
	
	var $httpBackend,
		$http,
		$rootScope,
		$compile,
		$templateCache,
		fragmentOne = '<div>#one</div>',
		fragmentTwo = '<div>#two</div>',
		fragment = '<div>#main</div>',
		template = fragment + '<script type="text/ng-template" id="/fragment.html#one">' + fragmentOne + '</script><script type="text/ng-template" id="/fragment.html#two">' + fragmentTwo + '</script>';
		
	function getTemplateData ( url ) {
		var result = $templateCache.get(url),
			template;
		
		if(angular.isArray(result)) {
			template = result[1];
		} else {
			template = result;
		}
		
		return template;
	}
	
	beforeEach(inject([ '$httpBackend', '$http', '$rootScope', '$compile', '$templateCache', function ( ) {
		
		$httpBackend = arguments[0];
		$http = arguments[1];
		$rootScope = arguments[2];
		$compile = arguments[3];
		$templateCache = arguments[4];
		
		$httpBackend.expectGET('/fragment.html')
			.respond(200, template);
		
	}]));
	
	it('should load the entire template', function ( ) {
		
		var tpl = '<div ng-include="\'/fragment.html\'"></div>',
			el = angular.element(tpl),
			result;
			
		$compile(el)($rootScope);
		
		$rootScope.$digest();
		
		$httpBackend.flush();
		
		result = getTemplateData('/fragment.html');
		
		expect(result).toBe(template);
		
	});
	
	it('should cache all templates in the loaded file', function ( ) {
		
		var tpl = '<div ng-include="\'/fragment.html\'"></div>',
			el = angular.element(tpl);
			
		$compile(el)($rootScope);
		
		$rootScope.$digest();
		
		$httpBackend.flush();
		
		expect(getTemplateData('/fragment.html')).toBe(template);
		expect(getTemplateData('/fragment.html#one')).toBe(fragmentOne);
		expect(getTemplateData('/fragment.html#two')).toBe(fragmentTwo);
		
	});
	
	it('should set the content of the file to the loaded fragment', function ( ) {
		
		var tpl = '<div><div ng-include="\'/fragment.html\'"></div></div>',
			scope = $rootScope.$new(),
			el,
			html;
			
		el = $compile(tpl)(scope);
			
		scope.$digest();
		
		$httpBackend.flush();
		
		html = el.children().eq(0).children()[0].innerHTML;
		
		expect(html).toBe('#main');
		
	});
	
	it('should set the content of the file to the targeted partial', function ( ) {
		
		var tpl = '<div><div ng-include="\'/fragment.html#one\'"></div></div>',
			scope = $rootScope.$new(),
			el,
			html;
			
		el = $compile(tpl)(scope);
		
		scope.$digest();
		
		$httpBackend.flush();
		
		html = el.children().eq(0).children()[0].innerHTML;
		
		expect(html).toBe('#one');
		
	});
	
	it('should set the correct contents if all are loaded simultaneously', function ( ) {
		
		var tpl = '<div><div ng-include="\'/fragment.html#one\'"></div><div ng-include="\'/fragment.html\'"></div><div ng-include="\'/fragment.html#two\'"></div></div>',
			scope = $rootScope.$new(),
			el;
			
		el = $compile(tpl)(scope);
		
		scope.$digest();
		
		$httpBackend.flush();
		
		expect(el.children().eq(0).children()[0].innerHTML).toBe('#one');
		expect(el.children().eq(1).children()[0].innerHTML).toBe('#main');
		expect(el.children().eq(2).children()[0].innerHTML).toBe('#two');
		
		
	});
  
});
